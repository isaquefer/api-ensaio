from django.db import models


class Naipe(models.Model):
    nome = models.CharField(max_length=80, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'naipe'
        unique_together = ['nome']


class Instrumento(models.Model):
    nome = models.CharField(max_length=80, blank=True, null=True)
    id_naipe = models.ForeignKey(
        'Naipe', models.DO_NOTHING, db_column='id_naipe',
        blank=True, null=True
    )

    class Meta:
        managed = True
        db_table = 'instrumento'
        unique_together = ['nome']


class Ensaio(models.Model):
    data_ensaio = models.DateField(unique=True, blank=True, null=True)
    encarregados = models.IntegerField(blank=True, null=True)
    organistas = models.IntegerField(blank=True, null=True)
    irmandade = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = True
        unique_together = ['data_ensaio']
        db_table = 'ensaio'


class EnsaioInstrumento(models.Model):
    id_instrumento = models.ForeignKey(
        'Instrumento', models.DO_NOTHING,
        db_column='id_instrumento', blank=True, null=True
    )

    id_ensaio = models.ForeignKey(
        Ensaio, models.DO_NOTHING, db_column='id_ensaio',
        blank=True, null=True
    )
    quantidade = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'ensaio_instrumento'
        unique_together = (('id_instrumento', 'id_ensaio'),)
