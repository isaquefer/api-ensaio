from rest_framework import serializers

from .models import (Naipe, Ensaio, Instrumento, EnsaioInstrumento)


class NaipeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Naipe
        fields = ['nome']


class InstrumentoSerializer(serializers.ModelSerializer):

    class Meta:
        model = Instrumento
        fields = ['nome', 'id_naipe']


class EnsaioSerializer(serializers.ModelSerializer):
    class Meta:
        model = Ensaio
        fields = [
            'data_ensaio',
            'encarregados',
            'organistas',
            'irmandade',
        ]


class EnsaioInstrumentoSerializer(serializers.ModelSerializer):
    class Meta:
        model = EnsaioInstrumento
        fields = [
            'id_instrumento',
            'id_ensaio',
            'quantidade',
        ]
