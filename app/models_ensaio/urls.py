from django.urls import path, include

from .views import (InstrumentoView, NaipeViewSet,
                    EnsaioViewSet, EnsaioInstrumentoView)
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'naipe', NaipeViewSet)
router.register(r'ensaio', EnsaioViewSet)
router.register(r'instrumento', InstrumentoView)
router.register(r'ensaio_instrumento', EnsaioInstrumentoView)

# The API URLs are now determined automatically by the router.
urlpatterns = [
    path('', include(router.urls)),
]
