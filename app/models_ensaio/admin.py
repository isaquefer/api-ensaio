from django.contrib import admin
from .models import Naipe, Instrumento, Ensaio, EnsaioInstrumento


class EnsaioAdmin(admin.ModelAdmin):
    pass


class NaipeAdmin(admin.ModelAdmin):
    pass


class InstrumentoAdmin(admin.ModelAdmin):
    pass


class EnsaioInstrumentoAdmin(admin.ModelAdmin):
    pass


admin.site.register(Ensaio, EnsaioAdmin)
admin.site.register(Naipe, NaipeAdmin)
admin.site.register(Instrumento, InstrumentoAdmin)
admin.site.register(EnsaioInstrumento, EnsaioInstrumentoAdmin)
# Register your models here.
