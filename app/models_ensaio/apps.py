from django.apps import AppConfig


class ModelsEnsaioConfig(AppConfig):
    name = 'models_ensaio'
