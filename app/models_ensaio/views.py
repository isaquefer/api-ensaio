from rest_framework.permissions import IsAuthenticated
from rest_framework import viewsets

from .models import Naipe, Ensaio, Instrumento, EnsaioInstrumento
from .serializers import (NaipeSerializer, EnsaioSerializer,
                          InstrumentoSerializer, EnsaioInstrumentoSerializer)


class NaipeViewSet(viewsets.ModelViewSet):

    queryset = Naipe.objects.all()
    serializer_class = NaipeSerializer
    permission_classes = [IsAuthenticated]


class EnsaioViewSet(viewsets.ModelViewSet):

    queryset = Ensaio.objects.all()
    serializer_class = EnsaioSerializer
    permission_classes = [IsAuthenticated]


class InstrumentoView(viewsets.ModelViewSet):

    queryset = Instrumento.objects.all()
    serializer_class = InstrumentoSerializer
    permission_classes = [IsAuthenticated]


class EnsaioInstrumentoView(viewsets.ModelViewSet):

    queryset = EnsaioInstrumento.objects.all()
    serializer_class = EnsaioInstrumentoSerializer
    permission_classes = [IsAuthenticated]
