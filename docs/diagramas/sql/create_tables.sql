create table ensaio (
  id SERIAL,
  data_ensaio date,
  encarregados int,
  organistas int,
  irmandade int,

  constraint unique_data_ensaio unique (data_ensaio),
  constraint pk_ensaio primary key (id)
);

create table naipe (
  id SERIAL,
  nome varchar(80),
  constraint pk_naipe primary key (id)
);

create table instrumento (
  id SERIAL,
  nome varchar(80),
  id_naipe int,

  constraint pk_instrumento primary key (id),
  constraint fk_instrumento_naipe foreign key (id_naipe) references naipe(id) ON UPDATE CASCADE ON DELETE CASCADE
);

create table ensaio_instrumento (
  id SERIAL,
  id_instrumento int,
  id_ensaio int,
  quantidade int,

  constraint fk_ensaioinstrumento_instrumento foreign key (id_instrumento) references instrumento(id) ON UPDATE CASCADE ON DELETE CASCADE,
  constraint fk_ensaioinstrumento_ensaio foreign key (id_ensaio) references ensaio(id) ON UPDATE CASCADE ON DELETE CASCADE,
  constraint unique_ensaioinstrumento unique (id_instrumento, id_ensaio),
  constraint pk_ensaio_instrumento primary key (id)
);
