## Como Executar o projeto

```
docker-compose build
```

### Pré-requisitos

* docker
* docker-compose
* python 3.8
* poetry

### Desenvolvimento
```
./up.sh TRUE
```

### Produção
```
./up.sh FALSE
```
